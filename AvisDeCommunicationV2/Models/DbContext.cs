﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AvisDeCommunicationV2.Models
{
    public class DBContext
    {
        /// <summary>
        /// Dictionnaire de strings de connexion.
        /// À l'intérieur se retrouvent nos strings de connexions à nos serveurs.
        /// </summary>
        Dictionary<string, string> _connectionStrings = new Dictionary<string, string>();

        public DBContext(/*string dsvicConnectionString,*/ string dsextConnectionString)
        {
            _connectionStrings.Add("DS-EXT", dsextConnectionString);
            //_connectionStrings.Add("DS-EXT", dsextConnectionString);
        }

        /// <summary>
        /// Dans la plupart des cas, on veut obtenir la connexion à notre serveur local, cette
        /// méthode appelle donc la méthode GetConnection avec en paramètre la "key" représentant
        /// notre serveur local.
        /// </summary>
        /// <returns>La connexion à notre serveur local.</returns>
        public MySqlConnection GetOpenDefaultConnection()
        {
            return GetOpenConnection("DS-EXT");
        }

        /// <summary>
        /// Cette méthode permet d'obtenir la connexion au serveur dont la "key" est passée en paramètre.
        /// En date du 3 juillet 2019, nous avons "DS-VIC" pour notre serveur local, et "DS-EXT" pour notre
        /// serveur distant.
        /// </summary>
        /// <param name="server">Clé représentant le serveur.</param>
        /// <returns>La connexion au serveur approprié.</returns>
        public MySqlConnection GetOpenConnection(string server)
        {
            MySqlConnection connection = new MySqlConnection(_connectionStrings[server]);
            connection.Open();
            return connection;
        }
    }
}
