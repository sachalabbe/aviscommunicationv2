﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AvisDeCommunicationV2.Models
{
    public class RoleAndPermissions
    {
        //Classe pour gérer les rôles et permissions
        //Ne sert que pour l'affichage de certaines données, à modifier
        
        public int? Role { get; set; }
        public bool Active { get; set; } = true;
        public string USR_Lang { get; set; } = "FR";
        public bool AllowToHire { get; set; }
        public string AccountName { get; set; }
    }
}
