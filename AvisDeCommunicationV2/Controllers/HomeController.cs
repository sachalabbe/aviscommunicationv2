﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AvisDeCommunicationV2.Models;
using MySql.Data.MySqlClient;

namespace AvisDeCommunicationV2.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            RoleAndPermissions RaP = new RoleAndPermissions();

            //Gérer la connexion par le lauchSettings.json ou appsettingDevelopment.json, pas en hardcode
            DBContext DBContext=new DBContext("server=GSM-DEV-DS-01;user id=root;password=PhiboperD1453");
            using (MySqlConnection connection = DBContext.GetOpenDefaultConnection())
            {
                MySqlCommand cmd;
                cmd = new MySqlCommand();
                cmd.Connection = connection;

                cmd.CommandText = $"SELECT AccountName, IsActive, UserRight, IsAllowToHire, LANG FROM hiringmateriel.tblusers AS users WHERE users.AccountName = @Username;";
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@Username", "vigneaultm");

                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        RaP.AccountName = Convert.ToString(reader["AccountName"]);
                        RaP.Active = Convert.ToBoolean(reader["IsActive"]);
                        RaP.AllowToHire = Convert.ToBoolean(reader["IsAllowToHire"]);
                        RaP.Role = Convert.ToInt16(reader["UserRight"]);
                        RaP.USR_Lang = Convert.ToString(reader["LANG"]);
                    }
                }
                ViewBag.rap = RaP;
            }
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
